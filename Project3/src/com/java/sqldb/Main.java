package com.java.sqldb;

import java.sql.*;


public class Main {

    public static void main(String[] args)
    {
        String url = "jdbc:sqlserver://DESKTOP-TICSAUK\\TEW_SQLEXPRESS;databaseName=CarSharing2;integratedSecurity=true";
        String query = "Select * from Samochod join Model on Samochod.id_modelu = Model.id_modelu  ";
        sqlServerDB db= new sqlServerDB(url,query);

        db.ListaAut.stream()
                .filter(g -> g.cena > 10)
                .map(g->g.model +" " + g.marka + " " + g.generacja)
                .forEach(System.out::println);
    }
}
