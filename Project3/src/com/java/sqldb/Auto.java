package com.java.sqldb;

public class Auto {
    public final String model;
    public final String marka;
    public final int moc;
    public final int m_obr;
    public final int generacja;
    public final int pr_maks;
    public final int l_cylindrow;
    public final int cena;

    public Auto(String Model, String Marka, int gen, int cen, int Moc, int M_Obr, int Pr_maks, int l_cyl)
    {
        this.model = Model;
        this.marka = Marka;
        this.moc = Moc;
        this.m_obr = M_Obr;
        this.generacja = gen;
        this.pr_maks = Pr_maks;
        this.l_cylindrow = l_cyl;
        this.cena =cen;
    }

}
