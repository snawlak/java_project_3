package com.java.wspolbieznosc;

import com.java.DataBases.Adult;
import com.java.DataBases.DataBaseInstance;
import com.java.DataBases.Employee;

import java.util.List;

public class App {
    public static void main (String [] args){
        DataBase dataBaseAdult = new DataBase(DataBase.DataBaseName.adult, new Adult());
        dataBaseAdult.createRecords();
        //dataBaseAdult.setFilter(Adult.Field.age.toString(),49,">");
        //dataBaseAdult.setFilter(Adult.Field.sallary.toString(),">50K");
        //dataBaseAdult.setFilter(Adult.Field.relationship.toString(),Adult.Relationship.Husband.toString());
        dataBaseAdult.setFilters(Adult.Field.relationship.toString()
                ,Adult.Relationship.Husband.toString()
                ,Adult.Relationship.Wife.toString());

        System.out.println(dataBaseAdult.count());
        System.out.println(dataBaseAdult.max(Adult.Field.age.toString()));
        System.out.println(dataBaseAdult.min(Adult.Field.age.toString()));
        System.out.printf("%.2f",dataBaseAdult.average(Adult.Field.age.toString())); // %.2f - two decimal places

        System.out.println();
        System.out.println("DATA BASE EMPLOYE ******");


        DataBase dataBaseEmployee = new DataBase(DataBase.DataBaseName.employee, new Employee());
        dataBaseEmployee.createRecords();
        dataBaseEmployee.setFilter(Employee.Field.employee_id.toString(),862, ">");
        System.out.println(dataBaseEmployee.count());


    }
}
