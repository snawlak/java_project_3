package com.java.wspolbieznosc;

import com.java.DataBases.Adult;
import com.java.DataBases.DataBaseInstance;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class FileOperation {

    private String dataBaseFileName;
    private String separator;

    FileOperation(String dataBaseFileName, String separator){
        this.dataBaseFileName = dataBaseFileName;
        this.separator = separator;
    }

    public  List<ArrayList<String>> getListOfRecords(){
        // Create temporary records as an linked list to faster adding new records
        List<ArrayList<String>> records = new LinkedList<>();
        // read from file
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(dataBaseFileName))) {
            String line = bufferedReader.readLine();
            while(line != null) {
                // save line as an array of data
                ArrayList<String> tempRecord = new ArrayList<> (Arrays.asList(line.split(separator)));
                // add temporary record to list of all records
                records.add(tempRecord);
                // read next line
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;

    }
}
