package com.java.wspolbieznosc;

import com.java.DataBases.Adult;
import com.java.DataBases.DataBaseInstance;
import com.java.DataBases.Employee;

import java.io.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataBase {

    private final String PATH_TO_DATABASE = "DataBase" + File.separator;
    private String dataBaseFileName;
    private DataBaseName dataBaseName;
    private DataBaseInstance dataBase;

    private Stream<DataBaseInstance> dataBaseInstanceStream;
    private List<DataBaseInstance> dataBaseInstanceRecords = new ArrayList<DataBaseInstance>();

    private List<DataBaseInstance> resultsList;
    private boolean resultsInitialized = false;

    // all database list - if we will have more databases
    public enum DataBaseName{
        adult, employee;
    }

    public DataBase(DataBaseName dataBaseName, DataBaseInstance dataBase){
        this.dataBaseFileName = PATH_TO_DATABASE + dataBaseName.toString() + dataBase.getFileType();
        this.dataBase = dataBase;

        if(dataBaseName.equals(DataBaseName.adult)){
            this.dataBaseName = DataBaseName.adult;
        }
        else if(dataBaseName.equals(DataBaseName.employee)){
            this.dataBaseName = DataBaseName.employee;
        }
    }

    public void createRecords(){

        FileOperation fileOperation = new FileOperation(dataBaseFileName, dataBase.getSeparator());
        List<ArrayList<String>> tempRecords = fileOperation.getListOfRecords();

        for(ArrayList<String> record : tempRecords){
            addToRecords(record);
        }
        dataBaseInstanceStream = dataBaseInstanceRecords.stream();
    }

    private void addToRecords(ArrayList<String> record){
        // check database
        if(this.dataBaseName.equals(DataBaseName.adult)){
            // add new Adult object with given record
            dataBaseInstanceRecords.add(new Adult(record));
        }
        else if(this.dataBaseName.equals(DataBaseName.employee)){
            // add new Employee object with given record
            dataBaseInstanceRecords.add(new Employee(record));
        }
    }
    /**
     * set filters on database with Integer values
     * @param field
     * @param valueToCompare
     * @param comparableSign
     */
    public void setFilter(String field, int valueToCompare, String comparableSign){
        switch (comparableSign){
            case "=":
                dataBaseInstanceStream = dataBaseInstanceStream.filter(dataBaseInstance ->
                        (Integer)dataBaseInstance.getField(field) == valueToCompare);
                break;
            case ">":
                dataBaseInstanceStream = dataBaseInstanceStream.filter(dataBaseInstance ->
                        (Integer)dataBaseInstance.getField(field) > valueToCompare);
                break;
            case "<":
                dataBaseInstanceStream = dataBaseInstanceStream.filter(dataBaseInstance ->
                        (Integer)dataBaseInstance.getField(field) < valueToCompare);
                break;
            case ">=":
                dataBaseInstanceStream = dataBaseInstanceStream.filter(dataBaseInstance ->
                        (Integer)dataBaseInstance.getField(field) >= valueToCompare);
                break;
            case "<=":
                dataBaseInstanceStream = dataBaseInstanceStream.filter(dataBaseInstance ->
                        (Integer)dataBaseInstance.getField(field) <= valueToCompare);
                break;
        }


    }

    /**
     * Set Filters on database with string values
     * @param field
     * @param value
     */
    public void setFilter(String field, String value){
        dataBaseInstanceStream = dataBaseInstanceStream
                .filter(dataBaseInstance -> dataBaseInstance.getField(field).equals(value));
    }

    public void showResults(){
        Stream<DataBaseInstance> filteredStream = dataBaseInstanceStream;

        Stream<String> namesStream = filteredStream
                .map(dataBaseInstance -> dataBaseInstance.getField(Adult.Field.nativeCountry.toString()).toString().toUpperCase());
        namesStream.forEach(System.out::println);
    }

    List<DataBaseInstance> getResult(){
        return  dataBaseInstanceStream.collect(Collectors.toList());
    }



    private void initResult(){
        resultsList = dataBaseInstanceStream.collect(Collectors.toList());
        resultsInitialized = true;
    }

    private List<Integer> getListOfFieldValue(String field){
        List<Integer> listOfFields = new ArrayList<>();
        if(!resultsInitialized){
            initResult();
        }
        for(DataBaseInstance dataBaseInstance: resultsList){
            listOfFields.add((Integer)dataBaseInstance.getField(field));
        }
        return listOfFields;
    }

    public List<String> getListOfFieldValueString(String field){
        List<String> listOfFields = new ArrayList<>();
        if(!resultsInitialized){
            initResult();
        }
        for(DataBaseInstance dataBaseInstance: resultsList){
            listOfFields.add((String)dataBaseInstance.getField(field));
        }
        return listOfFields;
    }


    public int count(){
        if(!resultsInitialized){
            initResult();
        }
        return resultsList.size();
    }

    public int max(String field){
        return Collections.max(getListOfFieldValue(field));
    }

    public int min(String field){
        return Collections.min(getListOfFieldValue(field));
    }

    public double average(String field){
        return getListOfFieldValue(field).stream().mapToInt(i -> i).average().orElse(0);
    }

    public void setFilters(String field, String value1, String value2){

        dataBaseInstanceStream = dataBaseInstanceStream
                .filter(dataBaseInstance ->
                        dataBaseInstance.getField(field).equals(value1) ||
                        dataBaseInstance.getField(field).equals(value2));
    }
}
