package com.java.DataBases;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Employee implements DataBaseInstance {

    public enum Field{
        id,employee_id,full_name,
        first_name,last_name,position_id,
        position_title,store_id, department_id,
        birth_date,hire_date,end_date,
        salary,supervisor_id,education_level,
        marital_status,gender,management_role;
    }
    private String id;
    private int employee_id;
    private String full_name;
    private String first_name;
    private String last_name;
    private int position_id;
    private String position_title;
    private int store_id;
    private int department_id;
    private String birth_date;
    private String hire_date;
    private String end_date;
    private int salary;
    private int supervisor_id;
    private String education_level;
    private String marital_status;
    private String gender;
    private String management_role;

    private Map<Field, Object> mapOfFields = new HashMap<>();

    public Employee(){

    }

    public Employee(ArrayList<String> fields){
        this.id = fields.get(0);
        this.employee_id = Integer.parseInt(fields.get(1));
        this.full_name = fields.get(2);
        this.first_name = fields.get(3);
        this.last_name = fields.get(4);
        this.position_id = Integer.parseInt(fields.get(5));
        this.position_title = fields.get(6);
        this.store_id = Integer.parseInt(fields.get(7));
        this.department_id = Integer.parseInt(fields.get(8));
        this.birth_date = fields.get(9);
        this.hire_date = fields.get(10);
        this.end_date = fields.get(11);
        this.salary = Integer.parseInt(fields.get(12));
        this.supervisor_id = Integer.parseInt(fields.get(13));
        this.education_level = fields.get(14);
        this.marital_status = fields.get(15);
        this.gender = fields.get(16);
        this.management_role = fields.get(17);
        saveToMap();
    }

    private void saveToMap(){
        mapOfFields.put(Field.id,id);
        mapOfFields.put(Field.employee_id,employee_id);
        mapOfFields.put(Field.full_name,full_name);
        mapOfFields.put(Field.first_name,first_name);
        mapOfFields.put(Field.last_name,last_name);
        mapOfFields.put(Field.position_id,position_id);
        mapOfFields.put(Field.position_title,position_title);
        mapOfFields.put(Field.store_id,store_id);
        mapOfFields.put(Field.department_id,department_id);
        mapOfFields.put(Field.birth_date,birth_date);
        mapOfFields.put(Field.hire_date,hire_date);
        mapOfFields.put(Field.end_date,end_date);
        mapOfFields.put(Field.salary,salary);
        mapOfFields.put(Field.supervisor_id,supervisor_id);
        mapOfFields.put(Field.education_level,education_level);
        mapOfFields.put(Field.marital_status,marital_status);
        mapOfFields.put(Field.gender,gender);
        mapOfFields.put(Field.management_role,management_role);
    }

    @Override
    public Object getField(String key) {
        Field field = getFieldFromString(key);
        return mapOfFields.get(field);
    }

    @Override
    public String getSeparator() {
        return ",";
    }
    @Override
    public String getFileType() {
        return ".csv";
    }

    private Field getFieldFromString(String strField){
        return Field.valueOf(strField);
    }


    public enum Gender{
        F, M;
    }

    public enum Education{
        Bachelors_Degree, Graduate_Degree, High_School_Degree, Partial_College, Partial_High_School;
    }

    public enum Marital_status{
        M, S;
    }
    public enum Occupation{
        HQ_Finance_and_Accounting,HQ_Human_Resources,HQ_Information_Systems,HQ_Marketing,President,Store_Assistant_Manager,Store_Information_Systems,Store_Manager,Store_Permanent_Butcher,Store_Permanent_Checker,Store_Permanent_Stocker,Store_Shift_Supervisor,Store_Temporary_Checker,Store_Temporary_Stocker,VP_Country_Manager,VP_Finance,VP_Human_Resources,VP_Information_Systems;
    }


}
