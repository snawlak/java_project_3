package com.java.DataBases;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Adult implements DataBaseInstance {

    public enum Field{
        age,workclass,flnwgt,education,educationNum,maritialStatus,occupation,relationship,
        race,sex,capitalGain,capitalLoss,hoursPerWeek,nativeCountry,sallary;
    }

    private int age;
    private String workclass;
    private String flnwgt;
    private String education;
    private int educationNum;
    private String maritialStatus;
    private String occupation;
    private String relationship;
    private String race;
    private String sex;
    private int capitalGain;
    private int capitalLoss;
    private int hoursPerWeek;
    private String nativeCountry;
    private String sallary;

    private Map<Adult.Field, Object> mapOfFields = new HashMap<>();

    public Adult(){

    }

    @Override
    public String getFileType() {
        return ".data";
    }

    public Adult(ArrayList<String> fields){
        this.age = Integer.parseInt(fields.get(0));
        this.workclass = fields.get(1);
        this.flnwgt = fields.get(2);
        this.education = fields.get(3);
        this.educationNum = Integer.parseInt(fields.get(4));
        this.maritialStatus = fields.get(5);
        this.occupation = fields.get(6);
        this.relationship = fields.get(7);
        this.race = fields.get(8);
        this.sex = fields.get(9);
        this.capitalGain = Integer.parseInt(fields.get(10));
        this.capitalLoss = Integer.parseInt(fields.get(11));
        this.hoursPerWeek = Integer.parseInt(fields.get(12));
        this.nativeCountry = fields.get(13);
        this.sallary = fields.get(14);
        saveToMap();
    }

    private void saveToMap(){
        mapOfFields.put(Field.age,age);
        mapOfFields.put(Field.workclass,workclass);
        mapOfFields.put(Field.flnwgt,flnwgt);
        mapOfFields.put(Field.education,education);
        mapOfFields.put(Field.educationNum,educationNum);
        mapOfFields.put(Field.maritialStatus,maritialStatus);
        mapOfFields.put(Field.occupation,occupation);
        mapOfFields.put(Field.relationship,relationship);
        mapOfFields.put(Field.race,race);
        mapOfFields.put(Field.sex,sex);
        mapOfFields.put(Field.capitalGain,capitalGain);
        mapOfFields.put(Field.capitalLoss,capitalLoss);
        mapOfFields.put(Field.hoursPerWeek,hoursPerWeek);
        mapOfFields.put(Field.nativeCountry,nativeCountry);
        mapOfFields.put(Field.sallary,sallary);
    }

    @Override
    public Object getField(String key) {
        Field field = getFieldFromString(key);
        return mapOfFields.get(field);
    }

    @Override
    public String getSeparator() {
        return ", ";
    }

    private Field getFieldFromString(String strField){
        return Field.valueOf(strField);
    }

    public enum Workclass{
        Private, Self_emp_not_inc, Self_emp_inc, Federal_gov, Local_gov, State_gov, Without_pay, Never_worked;
    }

    public enum Education{
        Bachelors, Some_college, HS_grad, Prof_school, Assoc_acdm, Assoc_voc, Masters,Doctorate, Preschool;
    }

    public enum Marital_status{
        Married_civ_spouse, Divorced, Never_married, Separated, Widowed, Married_spouse_absent, Married_AF_spouse;
    }
    public enum Occupation{
        Tech_support, Craft_repair, Other_service, Sales, Exec_managerial, Prof_specialty, Handlers_cleaners, Machine_op_inspct, Adm_clerical, Farming_fishing, Transport_moving, Priv_house_serv, Protective_serv, Armed_Forces;
    }

    public enum Relationship{
        Wife, Own_child, Husband, Not_in_family, Other_relative, Unmarried;
    }

    public enum Race{
        White, Asian_Pac_Islander, Amer_Indian_Eskimo, Other, Black;
    }

    public enum Sex{
        Female, Male;
    }

    public enum Native_country{
        United_States, Cambodia, England, Puerto_Rico, Canada, Germany, Outlying_US, India, Japan, Greece, South, China, Cuba, Iran, Honduras, Philippines, Italy, Poland, Jamaica, Vietnam, Mexico, Portugal, Ireland, France, Dominican_Republic, Laos, Ecuador, Taiwan, Haiti, Columbia, Hungary, Guatemala, Nicaragua, Scotland, Thailand, Yugoslavia, El_Salvador, Trinadad_Tobago, Peru, Hong, Holand_Netherlands;
    }

}
