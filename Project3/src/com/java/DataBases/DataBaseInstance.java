package com.java.DataBases;

public interface DataBaseInstance {
    Object getField(String key);
    String getSeparator();
    String getFileType();
}
