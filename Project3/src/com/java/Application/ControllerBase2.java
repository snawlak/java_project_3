package com.java.Application;

import com.java.sqldb.Auto;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ControllerBase2 extends GUI implements Initializable {
    public List<Auto> ListaAut = new ArrayList<Auto>();
    public TextField host;
    public TextField inst;
    public TextField dbName;


    @FXML
    protected void setToMainScreen(ActionEvent event) throws IOException
    {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("MainScreen.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


    public void Connect(ActionEvent event)
    {
        try {
            String url = "jdbc:sqlserver://"+host.getText()+"\\" +inst.getText() + ";databaseName="+dbName.getText()+";integratedSecurity=true";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection con = DriverManager.getConnection(url);
            Statement st = con.createStatement();
            String query = "Select * from Samochod join Model on Samochod.id_modelu = Model.id_modelu  ";
            ResultSet rs = st.executeQuery(query);


            while(rs.next())
            {
                String a = rs.getString("marka");
                String b = rs.getString("model");
                int c = rs.getInt("generacja");
                int d = rs.getInt("cena");
                int e =rs.getInt("moc");
                int f =rs.getInt("moment_obrotowy");
                int g =rs.getInt("maksymalna_predkosc");
                int h =rs.getInt("liczba_cylindrow");
                this.ListaAut.add(new Auto(a,b,c,d,e,f,g,h));
            }

        }catch(Exception e)
        {
            System.out.println(e);
        }

        ListaAut.stream()
                .filter(g -> g.cena > 10)
                .map(g->g.model +" " + g.marka + " " + g.generacja)
                .forEach(System.out::println);
    }

}
