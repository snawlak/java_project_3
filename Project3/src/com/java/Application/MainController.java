package com.java.Application;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitMenuButton;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController extends GUI  {


    @FXML
    protected void changeScreenButtonPushed(ActionEvent event) throws IOException {

        Button button = (Button) event.getSource();
        System.out.println(button.getId());
        switch (button.getId()) {
            case "buttonBase1":
                setScreen("Base1Screen.fxml", event);
                break;
            case "buttonBase2":
                setScreen("Base2Screen.fxml", event);
                break;
            case "buttonBase3":
                setScreen("Base3Screen.fxml", event);
                break;
            case "buttonBase4":
                setScreen("Base4Screen.fxml", event);
                break;
        }
    }

    @FXML
    protected void setToMainScreen(ActionEvent event) throws IOException
    {
        setScreen("MainScreen.fxml", event);
    }

    private void setScreen(String name, ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource(name));
        Scene tableViewScene = new Scene(tableViewParent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }
}
