package com.java.Application;

import com.java.DataBases.Adult;
import com.java.DataBases.Employee;
import com.java.wspolbieznosc.DataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Stream;

public class ControllerBase3 extends GUI implements Initializable {

    public ComboBox comboPlec;
    public ComboBox comboStan;
    public ComboBox comboWyksztalcenie;
    public ComboBox comboStanowisko;
    public TextField comboWiekMin;
    public TextField comboWiekMax;
    public TextField ResultAvg;
    public TextField ResultMin;
    public TextField ResultMax;
    public TextField ResultEmployee;

    @FXML
    protected void setToMainScreen(ActionEvent event) throws IOException
    {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("MainScreen.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }

    @FXML
    private void filterNow(ActionEvent actionEvent)
    {

        DataBase dataBaseEmployee = new DataBase(DataBase.DataBaseName.employee, new Employee());
        dataBaseEmployee.createRecords();
        setFilters(dataBaseEmployee);
        showResults(dataBaseEmployee);


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //------------------------------------------ Screen 1 begin

        comboWyksztalcenie.setItems(enumToOList(Employee.Education.values()));
        comboStan.setItems(enumToOList(Employee.Marital_status.values()));
        comboStanowisko.setItems(enumToOList(Employee.Occupation.values()));
        comboPlec.setItems(enumToOList(Employee.Gender.values()));

        comboWyksztalcenie.getSelectionModel().select(0);
        comboStan.getSelectionModel().select(0);
        comboStanowisko.getSelectionModel().select(0);
        comboPlec.getSelectionModel().select(0);


        //------------------------------------------ Screen 1 end
    }
    public ObservableList<String> enumToOList(Enum[] enumElement) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Wszystkie");
        for(Enum element: enumElement) {
            list.add(element.toString());
        }
        ObservableList<String> oList = FXCollections.observableArrayList(list);
        return oList;
    }

    private void setFilters(DataBase dataBase){

        if (!comboWyksztalcenie.getValue().toString().equals("Wszystkie")) {
            System.out.println("Wykształcenie - " + comboWyksztalcenie.getValue().toString());
            dataBase.setFilter(Adult.Field.education.toString(),comboWyksztalcenie.getValue().toString());
        }
        if (!comboPlec.getValue().toString().equals("Wszystkie")) {
            System.out.println("Płeć - " + comboPlec.getValue().toString());
            dataBase.setFilter(Employee.Field.gender.toString(),comboPlec.getValue().toString());
        }
        if (!comboStanowisko.getValue().toString().equals("Wszystkie")) {
            System.out.println("Stanowisko - " + comboStanowisko.getValue().toString());
            dataBase.setFilter(Employee.Field.position_title.toString(),comboStanowisko.getValue().toString());
        }
        if (!comboStan.getValue().toString().equals("Wszystkie")) {
            System.out.println("Stan - " + comboStan.getValue().toString());
            dataBase.setFilter(Employee.Field.marital_status.toString(),comboStan.getValue().toString());
        }

    }

    private void showResults(DataBase dataBase){

        int resultNumber = dataBase.count();
        double max_salary = dataBase.max(Employee.Field.salary.toString());
        double min_salary = dataBase.min(Employee.Field.salary.toString());
        double avg_salary = dataBase.average(Employee.Field.salary.toString());

        ResultAvg.setText(String.valueOf(avg_salary));
        ResultMax.setText(String.valueOf(max_salary));
        ResultMin.setText(String.valueOf(min_salary));
        ResultEmployee.setText(String.valueOf(resultNumber));
    }
}

