package com.java.Application;


import com.java.DataBases.Adult;
import com.java.wspolbieznosc.DataBase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Stream;

public class ControllerBase1 extends GUI implements Initializable {

    public ComboBox comboWorkclass;
    public ComboBox comboEducation;
    public ComboBox comboMarital_status;
    public ComboBox comboOccupation;
    public ComboBox comboRelationship;
    public ComboBox comboRace;
    public ComboBox comboSex;
    public ComboBox comboNative_country;
    public TextField ageUp;
    public TextField ageDown;
    public Label result;
    public Button filterButton;
    public TextField numberOfResults;
    public TextField resultsLessThan50k;
    public TextField resultsMoreThan50k;


    @FXML
    protected void setToMainScreen(ActionEvent event) throws IOException
    {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("MainScreen.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();
    }
    @FXML
    private void filterNow(ActionEvent actionEvent)
    {

        DataBase dataBaseAdult = new DataBase(DataBase.DataBaseName.adult, new Adult());
        dataBaseAdult.createRecords();
        setFilters(dataBaseAdult);
        showResults(dataBaseAdult);


    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //------------------------------------------ Screen 1 begin

        comboSex.setItems(enumToOList(Adult.Sex.values()));
        comboEducation.setItems(enumToOList(Adult.Education.values()));
        comboMarital_status.setItems(enumToOList(Adult.Marital_status.values()));
        comboNative_country.setItems(enumToOList(Adult.Native_country.values()));
        comboOccupation.setItems(enumToOList(Adult.Occupation.values()));
        comboRace.setItems(enumToOList(Adult.Race.values()));
        comboRelationship.setItems(enumToOList(Adult.Relationship.values()));
        comboWorkclass.setItems(enumToOList(Adult.Workclass.values()));
        comboSex.getSelectionModel().select(0);
        comboEducation.getSelectionModel().select(0);
        comboMarital_status.getSelectionModel().select(0);
        comboNative_country.getSelectionModel().select(0);
        comboOccupation.getSelectionModel().select(0);
        comboRace.getSelectionModel().select(0);
        comboRelationship.getSelectionModel().select(0);
        comboWorkclass.getSelectionModel().select(0);

        //------------------------------------------ Screen 1 end
    }
    public ObservableList<String> enumToOList(Enum[] enumElement) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Wszystkie");
        for(Enum element: enumElement) {
            list.add(element.toString());
        }
        ObservableList<String> oList = FXCollections.observableArrayList(list);
        return oList;
    }

    private void setFilters(DataBase dataBase){
        // AGE FILTERS
        dataBase.setFilter(Adult.Field.age.toString(),Integer.parseInt(ageDown.getText()),">=");
        dataBase.setFilter(Adult.Field.age.toString(),Integer.parseInt(ageUp.getText()),"<=");
        // OTHER FILTERS
        if (!comboWorkclass.getValue().toString().equals("Wszystkie")) {
            System.out.println("Workclass - " + comboWorkclass.getValue().toString());
            dataBase.setFilter(Adult.Field.workclass.toString(),comboWorkclass.getValue().toString());
        }
        if (!comboEducation.getValue().toString().equals("Wszystkie")) {
            System.out.println("Education - " + comboEducation.getValue().toString());
            dataBase.setFilter(Adult.Field.education.toString(),comboEducation.getValue().toString());
        }
        if (!comboMarital_status.getValue().toString().equals("Wszystkie")) {
            System.out.println("Marital_status - " + comboMarital_status.getValue().toString());
            dataBase.setFilter(Adult.Field.maritialStatus.toString(),comboMarital_status.getValue().toString());
        }
        if (!comboOccupation.getValue().toString().equals("Wszystkie")) {
            System.out.println("Occupation - " + comboOccupation.getValue().toString());
            dataBase.setFilter(Adult.Field.occupation.toString(),comboOccupation.getValue().toString());
        }
        if (!comboRelationship.getValue().toString().equals("Wszystkie")) {
            System.out.println("Relationship - " + comboRelationship.getValue().toString());
            dataBase.setFilter(Adult.Field.relationship.toString(),comboRelationship.getValue().toString());
        }
        if (!comboRace.getValue().toString().equals("Wszystkie")) {
            System.out.println("Race - " + comboRace.getValue().toString());
            dataBase.setFilter(Adult.Field.race.toString(),comboRace.getValue().toString());
        }
        if (!comboSex.getValue().toString().equals("Wszystkie")) {
            System.out.println("Sex - " + comboSex.getValue().toString());
            dataBase.setFilter(Adult.Field.sex.toString(),comboSex.getValue().toString());
        }
        if (!comboNative_country.getValue().toString().equals("Wszystkie")) {
            System.out.println("Native_country - " + comboNative_country.getValue().toString());
            dataBase.setFilter(Adult.Field.nativeCountry.toString(),comboNative_country.getValue().toString());
        }
    }

    private void showResults(DataBase dataBase){
        List<String> sallaries = dataBase.getListOfFieldValueString(Adult.Field.sallary.toString());
        NumberFormat formatter = new DecimalFormat("#0.00");
        int resultsNumber = sallaries.size();
        long moreThan50kResultNumber;
        double moreThan50K;
        double lessThan50K;

        Stream<String> sallariesStream = sallaries.stream();
        sallariesStream = sallariesStream
                .filter(sallary -> sallary.equals(">50K"));

        moreThan50kResultNumber = sallariesStream.count();

        moreThan50K = moreThan50kResultNumber/(double)resultsNumber * 100;
        lessThan50K = (resultsNumber - moreThan50kResultNumber)/(double)resultsNumber * 100;

        numberOfResults.setText(String.valueOf(resultsNumber));
        resultsMoreThan50k.setText(formatter.format(moreThan50K) + "%");
        resultsLessThan50k.setText(formatter.format(lessThan50K) + "%");
    }


}
